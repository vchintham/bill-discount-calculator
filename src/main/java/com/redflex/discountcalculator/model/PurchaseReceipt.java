package com.redflex.discountcalculator.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchaseReceipt {
	ShoppingBasket shoppingBasket;
	Double totalPrice;
	Double totalDiscount;
	
	public ShoppingBasket getShoppingBasket() {
		return shoppingBasket;
	}
	public void setShoppingBasket(ShoppingBasket shoppingBasket) {
		this.shoppingBasket = shoppingBasket;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public PurchaseReceipt(ShoppingBasket shoppingBasket, Double totalPrice, Double totalDiscount) {
		this.shoppingBasket = shoppingBasket;
		this.totalPrice = totalPrice;
		this.totalDiscount = totalDiscount;
	}
	
	public PurchaseReceipt() {
	}
	public void calCulateTotalPrice(ShoppingBasket shoppingBasket) {
		List<SKU> items = shoppingBasket.getItems();
		Double totalPrice = 0.0;
		for(SKU sku: items) {
			totalPrice += sku.getPrice();
		}
		setTotalPrice(totalPrice);
	}
	
    public void calCulateTotalSavings(ShoppingBasket shoppingBasket) {
    	List<SKU> items = shoppingBasket.getItems();
		Double totalPrice = 0.0;
		Double totalDiscountedPrice = 0.0;
		for(SKU sku: items) {
			totalPrice += sku.getPrice();
			totalDiscountedPrice += sku.getDiscountedPrice();
		}
		setTotalDiscount(totalPrice - totalDiscountedPrice);
	}
	public void printPurchaseReceipt() {
		shoppingBasket.getItems().stream().forEach(item -> {
			String cleranceCategory = item.isCleranceSale() == true ? "clearance" : null;
			String receiptString = "1" + cleranceCategory  + item.getName() + "at" + item.getPrice();
			System.out.println(receiptString);
		});
		System.out.println(totalPrice);
		System.out.println(totalDiscount);
	}
	
}
