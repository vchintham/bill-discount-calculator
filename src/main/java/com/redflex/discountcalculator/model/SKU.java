package com.redflex.discountcalculator.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SKU {
	public SKU(String id, String name, Category category, Double price, Double discountedPrice, boolean cleranceSale) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price;
		this.discountedPrice = discountedPrice;
		this.cleranceSale = false;
	}
	public boolean isCleranceSale() {
		return cleranceSale;
	}
	public void setCleranceSale(boolean cleranceSale) {
		this.cleranceSale = cleranceSale;
	}
	private String id;
	private String name;
	private Category category;
	private Double price;
	private Double discountedPrice;
	private boolean cleranceSale;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getDiscountedPrice() {
		return discountedPrice;
	}
	public void setDiscountedPrice(Double discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
}
