package com.redflex.discountcalculator.model;

/* TODO Category should be read from the Database */
public enum Category {
	BOOK,
	FOOD,
	DRINK,
	CLOTHS,
	OTHERS,
	CLEARANCE
}
