package com.redflex.discountcalculator.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShoppingBasket {
	List<SKU> items;

	public List<SKU> getItems() {
		return items;
	}

	public void setItems(List<SKU> skus) {
		this.items = skus;
	}
}
