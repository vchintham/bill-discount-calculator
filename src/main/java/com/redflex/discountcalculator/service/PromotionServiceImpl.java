package com.redflex.discountcalculator.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.redflex.discountcalculator.model.Category;
import com.redflex.discountcalculator.model.PurchaseReceipt;
import com.redflex.discountcalculator.model.ShoppingBasket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class PromotionServiceImpl implements PromotionService {
	
	@Autowired
	DiscountService discountService;
	
	@Getter
	@Setter
	@AllArgsConstructor
	public static class CurrentPromotions {
		Category category;
		Double discount;
		public Category getCategory() {
			return category;
		}
		public void setCategory(Category category) {
			this.category = category;
		}
		public Double getDiscount() {
			return discount;
		}
		public void setDiscount(Double discount) {
			this.discount = discount;
		}
		public CurrentPromotions(Category category, Double discount) {
			this.category = category;
			this.discount = discount;
		}
		
	}

	Map<Category, Double> promotionOffers;
	
	@Override
	public void init() {
		// TODO should load Discount on Category from Database
		List<CurrentPromotions> allPromotions = Arrays.asList(
				      new CurrentPromotions(Category.BOOK, 5.0),
				      new CurrentPromotions(Category.FOOD, 5.0),
				      new CurrentPromotions(Category.DRINK, 5.0),
				      new CurrentPromotions(Category.CLOTHS, 20.0),
				      new CurrentPromotions(Category.OTHERS, 3.0),
				      new CurrentPromotions(Category.CLEARANCE, 20.0));
		
		if(CollectionUtils.isNotEmpty(allPromotions)) {
			allPromotions.stream().forEach(promotion -> {
				promotionOffers.put(promotion.category, promotion.discount);
			});
		}
		
	}
	
	@Override
	public void applyDiscount(ShoppingBasket shoppingBasket, Map<Category, Double> promotionOffers) {
		discountService.applyDiscount(shoppingBasket, promotionOffers);
	}

	@Override
	public PurchaseReceipt checkForPromotions(ShoppingBasket shoppingBasket) {
		applyDiscount(shoppingBasket, promotionOffers);
		
		PurchaseReceipt purchaseReceipt = new PurchaseReceipt();
		purchaseReceipt.setShoppingBasket(shoppingBasket);
		purchaseReceipt.calCulateTotalPrice(shoppingBasket);
		purchaseReceipt.calCulateTotalSavings(shoppingBasket);
		purchaseReceipt.printPurchaseReceipt();
		
		return purchaseReceipt;
	}
	
}
