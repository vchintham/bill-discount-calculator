package com.redflex.discountcalculator.service;

import java.util.Map;

import com.redflex.discountcalculator.model.Category;
import com.redflex.discountcalculator.model.PurchaseReceipt;
import com.redflex.discountcalculator.model.ShoppingBasket;

public interface PromotionService {
	public void init();
	public PurchaseReceipt checkForPromotions(ShoppingBasket shoppingBasket);
	public void applyDiscount(ShoppingBasket shoppingBasket, Map<Category, Double> promotionOffers);
}
