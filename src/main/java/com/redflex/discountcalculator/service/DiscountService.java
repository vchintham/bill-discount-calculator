package com.redflex.discountcalculator.service;

import java.util.Map;

import com.redflex.discountcalculator.model.Category;
import com.redflex.discountcalculator.model.ShoppingBasket;

/**
 * @author vijaychintham27
 */
public interface DiscountService {
	void applyDiscount(ShoppingBasket shoppingBasket, Map<Category, Double> promotionOffers);
}
