package com.redflex.discountcalculator.service;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.redflex.discountcalculator.model.PurchaseReceipt;
import com.redflex.discountcalculator.model.ShoppingBasket;
import com.redflex.discountcalculator.utils.DataUtils;

public class SuperMarketServiceImpl implements SuperMarketService {
	
	private static final SuperMarketServiceImpl instance = new SuperMarketServiceImpl();
	
	DataUtils dataUtil = new DataUtils();
    
	@Autowired
	DiscountService discountService;
	
	@Autowired
	PromotionService promotionService;
	
	public static SuperMarketServiceImpl getInstance(){
		return instance;
	}
	
	private SuperMarketServiceImpl() {
	}
	
	@Override
	public PurchaseReceipt purchaseItems(ShoppingBasket shoppingBasket) {
		return checkAndApplyPromotion(shoppingBasket);
	}

	@Override
	public PurchaseReceipt provideReceipt(ShoppingBasket shoppingBasket) {
		return promotionService.checkForPromotions(shoppingBasket);
	}

	@Override
	public PurchaseReceipt checkAndApplyPromotion(ShoppingBasket shoppingBasket) {
		return null;
	}
	
	@Override
	public void shopItems(ShoppingBasket shoppingBasket) {
		dataUtil.readUserInput(shoppingBasket);
		purchaseItems(shoppingBasket);
		
	}

	public void start() {
		promotionService.init();
		shopItems(new ShoppingBasket());
	}

}
