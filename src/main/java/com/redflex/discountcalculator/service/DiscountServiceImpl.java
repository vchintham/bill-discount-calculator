package com.redflex.discountcalculator.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.redflex.discountcalculator.model.Category;
import com.redflex.discountcalculator.model.ShoppingBasket;

@Service("discountService")
public class DiscountServiceImpl implements DiscountService {

	@Override
	public void applyDiscount(ShoppingBasket shoppingBasket, Map<Category, Double> promotionOffers) {
		shoppingBasket.getItems().stream().forEach( item -> {
			item.setDiscountedPrice(getDiscount(item.getPrice(), promotionOffers.get(item.getCategory())));
		});
	}
	
	private Double getDiscount(Double price, Double discount) {
		price = price - (price * (discount / 100.0f));
		return price;
	}

}
