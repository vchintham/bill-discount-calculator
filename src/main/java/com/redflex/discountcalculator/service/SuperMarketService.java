/**
 * 
 */
package com.redflex.discountcalculator.service;

import com.redflex.discountcalculator.model.PurchaseReceipt;
import com.redflex.discountcalculator.model.ShoppingBasket;

/**
 * @author vijaychintham27
 *
 */
public interface SuperMarketService {
	public PurchaseReceipt purchaseItems(ShoppingBasket shoppingBasket);
	public PurchaseReceipt provideReceipt(ShoppingBasket shoppingBasket);
	public PurchaseReceipt checkAndApplyPromotion(ShoppingBasket shoppingBasket);
	public void shopItems(ShoppingBasket shoppingBasket);
}
