package com.redflex.discountcalculator.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.redflex.discountcalculator.model.Category;
import com.redflex.discountcalculator.model.SKU;
import com.redflex.discountcalculator.model.ShoppingBasket;

import lombok.extern.slf4j.Slf4j;

/**
 * @author vijaychintham27
 */
@Slf4j
public class DataUtils {

    private static final int QUANTITY_POSITION = 0;

	public ShoppingBasket readUserInput(ShoppingBasket shoppingBasket) {
    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    	String itemString;
    	
    	try {
			while ((itemString = br.readLine()) != null) {
				/* Good idea to get the parsing rules from the Database */
			    String[] inputTokens = StringUtils.split(StringUtils.trim(itemString));
			    List<String> inputListTokens = new ArrayList<String>();
			    CollectionUtils.addAll(inputListTokens, inputTokens);
			    Integer itemQuantity = Integer.valueOf(inputListTokens.get(0));
			    Double itemPrice = Double.valueOf(inputListTokens.get(inputListTokens.size() -1));
			    /*have to check for ignore case */
			    Category itemCategory = Category.valueOf(inputListTokens.stream().filter(p -> p.equals("book")).findAny().orElse(null));
			    
			    /* Run the logic to findout category */
			    
			    /* Create a list of identifiers which convert generic category to specific category for example shirt to Category.CLOTHS */
			    
			    SKU shoppingItem = new SKU(null, null, itemCategory, itemPrice, null, false);
			    shoppingBasket.getItems().add(shoppingItem);
			}
		} catch (IOException e) {
			System.out.println("Invalid Input");
			e.printStackTrace();
		}
    	
    	return shoppingBasket;
    }
}
