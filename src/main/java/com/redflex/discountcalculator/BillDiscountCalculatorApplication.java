package com.redflex.discountcalculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.redflex.discountcalculator.service.DiscountService;
import com.redflex.discountcalculator.service.DiscountServiceImpl;
import com.redflex.discountcalculator.service.SuperMarketServiceImpl;

@SpringBootApplication
public class BillDiscountCalculatorApplication {
	
    public static void main(String[] args) {
        SpringApplication.run(BillDiscountCalculatorApplication.class, args);
        SuperMarketServiceImpl.getInstance().start();
    }
}
